require('dotenv').config()
module.exports = {
	sql: {
		host: process.env.DB_HOST,
		port: process.env.DB_PORT,
		database: process.env.DB_NAME,
		username: process.env.DB_USER,
		password: process.env.DB_PASS,
		dialect: 'mysql',
		logging: true,
		timezone: '+07:00',
	},
	seedDB: false,
	seedDBForce: false,
	db: 'sql'
}
